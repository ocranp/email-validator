package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class EmailValidatorTest {
	
	@Test
	public void testIsValidEmailRegular() {
		assertTrue(EmailValidator.isValidEmail("portiaocran3@gmail.com"));
	}
	
	@Test
	public void testIsValidEmailException() {
		assertFalse(EmailValidator.isValidEmail(""));
	}
	
	@Test
	public void testIsValidEmailBoundaryIn() {
		assertTrue(EmailValidator.isValidEmail("por@gma.ca"));
	}
	
	@Test
	public void testIsValidEmailAtSymbolRegular() {
		assertTrue(EmailValidator.isValidEmail("portiaocran3@gmail.com"));
	}
	
	@Test
	public void testIsValidEmailAtSymbolBoundaryIn() {
		assertTrue(EmailValidator.isValidEmail("portiaocran3@gmail.com"));
	}
	
	
	@Test
	public void testIsValidEmailAtSymbolBoundaryOut() {
		assertFalse(EmailValidator.isValidEmail("portiaocrangmail.com"));
	}
	
	@Test
	public void testIsValidEmailAccountRegular() {
		assertTrue("Invalid Email", EmailValidator.isValidEmail("portia123@gmail.com"));
	}
	
	@Test
	public void testIsValidEmailAccountBoundaryIn() {
		assertTrue("Invalid Email", EmailValidator.isValidEmail("por@gmail.com"));
	}
	
	
	@Test
	public void testIsValidEmailAccountBoundaryOut() {
		assertFalse("Valid Email", EmailValidator.isValidEmail("po@gmail.com"));
	}
	
	@Test
	public void testIsValidEmailDomainRegular() {
		assertTrue("Invalid Email", EmailValidator.isValidEmail("portiaocran@gmail.com"));
	}
	
	@Test
	public void testIsValidEmailDomainBoundaryIn() {
		assertTrue("Invalid Email", EmailValidator.isValidEmail("portiaocran@gma.com"));
	}
	
	@Test
	public void testIsValidEmailDomainBoundaryOut() {
		assertFalse("Invalid Email", EmailValidator.isValidEmail("portiaocran@ga.com"));
	}
	
	@Test
	public void testIsValidEmailExtensionRegular() {
		assertTrue("Invalid Email", EmailValidator.isValidEmail("portiaocran@gmail.com"));
	}
	
	@Test
	public void testIsValidEmailExtensionBoundaryIn() {
		assertTrue("Invalid Email", EmailValidator.isValidEmail("portiaocran@gmail.ca"));
	}
	
	@Test
	public void testIsValidEmailExtensionBoundaryOut() {
		assertFalse("Invalid Email", EmailValidator.isValidEmail("portiaocran@gmail.c"));
	}


}
