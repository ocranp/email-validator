package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {

	public static boolean isValidEmail(String email) {

		if (email == "" || email.isEmpty()) {
			return false;
		}

		Pattern emailRegex = Pattern.compile("^[a-z][a-z0-9]{2,}@[a-z0-9]{3,}.[a-z]{2,}$");
		Matcher matcher = emailRegex.matcher(email);
		return matcher.find();
	}

}
